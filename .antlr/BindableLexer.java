// Generated from c:\Users\Scott\Workspace\Bindable\Bindable.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BindableLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, IDENTIFIER=9, 
		WS=10, ESCAPEDQUOTE=11, QUOTE=12, DOUBLE_QUOTE=13, TRUE=14, FALSE=15, 
		NULL=16, Digits=17, QuotedString=18;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "IDENTIFIER", 
		"Available_identifier", "Identifier_or_keyword", "Identifier_start_character", 
		"Identifier_part_character", "Letter_character", "Combining_character", 
		"Decimal_digit_character", "Connecting_character", "Formatting_character", 
		"UNICODE_CLASS_LU", "UNICODE_CLASS_LL", "UNICODE_CLASS_LT", "UNICODE_CLASS_LM", 
		"UNICODE_CLASS_LO", "UNICODE_CLASS_NL", "UNICODE_CLASS_MN", "UNICODE_CLASS_MC", 
		"UNICODE_CLASS_CF", "UNICODE_CLASS_PC", "UNICODE_CLASS_ND", "WS", "ESCAPEDQUOTE", 
		"QUOTE", "DOUBLE_QUOTE", "TRUE", "FALSE", "NULL", "Digits", "QuotedString"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'-'", "'.'", "':'", "'('", "')'", "','", "'['", "']'", null, null, 
		null, "'''", "'\"'", "'x:True'", "'x:False'", "'x:Null'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, "IDENTIFIER", "WS", 
		"ESCAPEDQUOTE", "QUOTE", "DOUBLE_QUOTE", "TRUE", "FALSE", "NULL", "Digits", 
		"QuotedString"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public BindableLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Bindable.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\24\u00df\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31"+
		"\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t"+
		" \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\3\2\3\2\3\3\3\3\3\4\3"+
		"\4\3\5\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\7"+
		"\ff\n\f\f\f\16\fi\13\f\3\r\3\r\5\rm\n\r\3\16\3\16\3\16\3\16\3\16\5\16"+
		"t\n\16\3\17\3\17\3\17\3\17\3\17\3\17\5\17|\n\17\3\20\3\20\5\20\u0080\n"+
		"\20\3\21\3\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3"+
		"\27\3\30\3\30\3\31\3\31\3\32\3\32\3\33\3\33\3\34\3\34\3\35\3\35\3\36\3"+
		"\36\3\37\6\37\u009f\n\37\r\37\16\37\u00a0\3\37\3\37\3 \3 \3 \3 \5 \u00a9"+
		"\n \3!\3!\3\"\3\"\3#\3#\3#\3#\3#\3#\3#\3$\3$\3$\3$\3$\3$\3$\3$\3%\3%\3"+
		"%\3%\3%\3%\3%\3&\6&\u00c6\n&\r&\16&\u00c7\3\'\3\'\3\'\7\'\u00cd\n\'\f"+
		"\'\16\'\u00d0\13\'\3\'\3\'\3\'\3\'\3\'\7\'\u00d7\n\'\f\'\16\'\u00da\13"+
		"\'\3\'\3\'\5\'\u00de\n\'\4\u00ce\u00d8\2(\3\3\5\4\7\5\t\6\13\7\r\b\17"+
		"\t\21\n\23\13\25\2\27\2\31\2\33\2\35\2\37\2!\2#\2%\2\'\2)\2+\2-\2/\2\61"+
		"\2\63\2\65\2\67\29\2;\2=\f?\rA\16C\17E\20G\21I\22K\23M\24\3\2\n\4\2C\\"+
		"\u00c2\u00e0\6\2\u01c7\u01c7\u01ca\u01ca\u01cd\u01cd\u01f4\u01f4\5\2\u01bd"+
		"\u01bd\u01c2\u01c5\u0296\u0296\4\2\u16f0\u16f2\u2162\u2171\5\2\u0905\u0905"+
		"\u0940\u0942\u094b\u094e\5\2\u00af\u00af\u0602\u0605\u06df\u06df\b\2a"+
		"a\u2041\u2042\u2056\u2056\ufe35\ufe36\ufe4f\ufe51\uff41\uff41\4\2\13\13"+
		"\"\"\2\u00de\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2"+
		"\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2=\3\2\2\2\2?"+
		"\3\2\2\2\2A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2"+
		"\2\2\2M\3\2\2\2\3O\3\2\2\2\5Q\3\2\2\2\7S\3\2\2\2\tU\3\2\2\2\13W\3\2\2"+
		"\2\rY\3\2\2\2\17[\3\2\2\2\21]\3\2\2\2\23_\3\2\2\2\25a\3\2\2\2\27c\3\2"+
		"\2\2\31l\3\2\2\2\33s\3\2\2\2\35{\3\2\2\2\37\177\3\2\2\2!\u0081\3\2\2\2"+
		"#\u0083\3\2\2\2%\u0085\3\2\2\2\'\u0087\3\2\2\2)\u0089\3\2\2\2+\u008b\3"+
		"\2\2\2-\u008d\3\2\2\2/\u008f\3\2\2\2\61\u0091\3\2\2\2\63\u0093\3\2\2\2"+
		"\65\u0095\3\2\2\2\67\u0097\3\2\2\29\u0099\3\2\2\2;\u009b\3\2\2\2=\u009e"+
		"\3\2\2\2?\u00a8\3\2\2\2A\u00aa\3\2\2\2C\u00ac\3\2\2\2E\u00ae\3\2\2\2G"+
		"\u00b5\3\2\2\2I\u00bd\3\2\2\2K\u00c5\3\2\2\2M\u00dd\3\2\2\2OP\7/\2\2P"+
		"\4\3\2\2\2QR\7\60\2\2R\6\3\2\2\2ST\7<\2\2T\b\3\2\2\2UV\7*\2\2V\n\3\2\2"+
		"\2WX\7+\2\2X\f\3\2\2\2YZ\7.\2\2Z\16\3\2\2\2[\\\7]\2\2\\\20\3\2\2\2]^\7"+
		"_\2\2^\22\3\2\2\2_`\5\25\13\2`\24\3\2\2\2ab\5\27\f\2b\26\3\2\2\2cg\5\31"+
		"\r\2df\5\33\16\2ed\3\2\2\2fi\3\2\2\2ge\3\2\2\2gh\3\2\2\2h\30\3\2\2\2i"+
		"g\3\2\2\2jm\5\35\17\2km\7a\2\2lj\3\2\2\2lk\3\2\2\2m\32\3\2\2\2nt\5\35"+
		"\17\2ot\5!\21\2pt\5#\22\2qt\5\37\20\2rt\5%\23\2sn\3\2\2\2so\3\2\2\2sp"+
		"\3\2\2\2sq\3\2\2\2sr\3\2\2\2t\34\3\2\2\2u|\5\'\24\2v|\5)\25\2w|\5+\26"+
		"\2x|\5-\27\2y|\5/\30\2z|\5\61\31\2{u\3\2\2\2{v\3\2\2\2{w\3\2\2\2{x\3\2"+
		"\2\2{y\3\2\2\2{z\3\2\2\2|\36\3\2\2\2}\u0080\5\63\32\2~\u0080\5\65\33\2"+
		"\177}\3\2\2\2\177~\3\2\2\2\u0080 \3\2\2\2\u0081\u0082\5;\36\2\u0082\""+
		"\3\2\2\2\u0083\u0084\59\35\2\u0084$\3\2\2\2\u0085\u0086\5\67\34\2\u0086"+
		"&\3\2\2\2\u0087\u0088\t\2\2\2\u0088(\3\2\2\2\u0089\u008a\4c|\2\u008a*"+
		"\3\2\2\2\u008b\u008c\t\3\2\2\u008c,\3\2\2\2\u008d\u008e\4\u02b2\u02f0"+
		"\2\u008e.\3\2\2\2\u008f\u0090\t\4\2\2\u0090\60\3\2\2\2\u0091\u0092\t\5"+
		"\2\2\u0092\62\3\2\2\2\u0093\u0094\4\u0302\u0312\2\u0094\64\3\2\2\2\u0095"+
		"\u0096\t\6\2\2\u0096\66\3\2\2\2\u0097\u0098\t\7\2\2\u00988\3\2\2\2\u0099"+
		"\u009a\t\b\2\2\u009a:\3\2\2\2\u009b\u009c\4\62;\2\u009c<\3\2\2\2\u009d"+
		"\u009f\t\t\2\2\u009e\u009d\3\2\2\2\u009f\u00a0\3\2\2\2\u00a0\u009e\3\2"+
		"\2\2\u00a0\u00a1\3\2\2\2\u00a1\u00a2\3\2\2\2\u00a2\u00a3\b\37\2\2\u00a3"+
		">\3\2\2\2\u00a4\u00a5\7`\2\2\u00a5\u00a9\7$\2\2\u00a6\u00a7\7`\2\2\u00a7"+
		"\u00a9\7)\2\2\u00a8\u00a4\3\2\2\2\u00a8\u00a6\3\2\2\2\u00a9@\3\2\2\2\u00aa"+
		"\u00ab\7)\2\2\u00abB\3\2\2\2\u00ac\u00ad\7$\2\2\u00adD\3\2\2\2\u00ae\u00af"+
		"\7z\2\2\u00af\u00b0\7<\2\2\u00b0\u00b1\7V\2\2\u00b1\u00b2\7t\2\2\u00b2"+
		"\u00b3\7w\2\2\u00b3\u00b4\7g\2\2\u00b4F\3\2\2\2\u00b5\u00b6\7z\2\2\u00b6"+
		"\u00b7\7<\2\2\u00b7\u00b8\7H\2\2\u00b8\u00b9\7c\2\2\u00b9\u00ba\7n\2\2"+
		"\u00ba\u00bb\7u\2\2\u00bb\u00bc\7g\2\2\u00bcH\3\2\2\2\u00bd\u00be\7z\2"+
		"\2\u00be\u00bf\7<\2\2\u00bf\u00c0\7P\2\2\u00c0\u00c1\7w\2\2\u00c1\u00c2"+
		"\7n\2\2\u00c2\u00c3\7n\2\2\u00c3J\3\2\2\2\u00c4\u00c6\5!\21\2\u00c5\u00c4"+
		"\3\2\2\2\u00c6\u00c7\3\2\2\2\u00c7\u00c5\3\2\2\2\u00c7\u00c8\3\2\2\2\u00c8"+
		"L\3\2\2\2\u00c9\u00ce\5A!\2\u00ca\u00cd\5? \2\u00cb\u00cd\13\2\2\2\u00cc"+
		"\u00ca\3\2\2\2\u00cc\u00cb\3\2\2\2\u00cd\u00d0\3\2\2\2\u00ce\u00cf\3\2"+
		"\2\2\u00ce\u00cc\3\2\2\2\u00cf\u00d1\3\2\2\2\u00d0\u00ce\3\2\2\2\u00d1"+
		"\u00d2\5A!\2\u00d2\u00de\3\2\2\2\u00d3\u00d8\5C\"\2\u00d4\u00d7\5? \2"+
		"\u00d5\u00d7\13\2\2\2\u00d6\u00d4\3\2\2\2\u00d6\u00d5\3\2\2\2\u00d7\u00da"+
		"\3\2\2\2\u00d8\u00d9\3\2\2\2\u00d8\u00d6\3\2\2\2\u00d9\u00db\3\2\2\2\u00da"+
		"\u00d8\3\2\2\2\u00db\u00dc\5C\"\2\u00dc\u00de\3\2\2\2\u00dd\u00c9\3\2"+
		"\2\2\u00dd\u00d3\3\2\2\2\u00deN\3\2\2\2\20\2gls{\177\u00a0\u00a8\u00c7"+
		"\u00cc\u00ce\u00d6\u00d8\u00dd\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}