// Generated from c:\Users\Scott\Workspace\Bindable\Bindable.g4 by ANTLR 4.7.1
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class BindableParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, IDENTIFIER=9, 
		WS=10, ESCAPEDQUOTE=11, QUOTE=12, DOUBLE_QUOTE=13, TRUE=14, FALSE=15, 
		NULL=16, Digits=17, QuotedString=18;
	public static final int
		RULE_program = 0, RULE_decimal_value = 1, RULE_boolean_value = 2, RULE_namespace_qualifier = 3, 
		RULE_static_type = 4, RULE_attached_expr = 5, RULE_cast_expr = 6, RULE_function = 7, 
		RULE_path = 8, RULE_function_param = 9;
	public static final String[] ruleNames = {
		"program", "decimal_value", "boolean_value", "namespace_qualifier", "static_type", 
		"attached_expr", "cast_expr", "function", "path", "function_param"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'-'", "'.'", "':'", "'('", "')'", "','", "'['", "']'", null, null, 
		null, "'''", "'\"'", "'x:True'", "'x:False'", "'x:Null'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, "IDENTIFIER", "WS", 
		"ESCAPEDQUOTE", "QUOTE", "DOUBLE_QUOTE", "TRUE", "FALSE", "NULL", "Digits", 
		"QuotedString"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Bindable.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public BindableParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgramContext extends ParserRuleContext {
		public PathContext path() {
			return getRuleContext(PathContext.class,0);
		}
		public TerminalNode EOF() { return getToken(BindableParser.EOF, 0); }
		public ProgramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_program; }
	}

	public final ProgramContext program() throws RecognitionException {
		ProgramContext _localctx = new ProgramContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_program);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(20);
			path(0);
			setState(21);
			match(EOF);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Decimal_valueContext extends ParserRuleContext {
		public List<TerminalNode> Digits() { return getTokens(BindableParser.Digits); }
		public TerminalNode Digits(int i) {
			return getToken(BindableParser.Digits, i);
		}
		public Decimal_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_decimal_value; }
	}

	public final Decimal_valueContext decimal_value() throws RecognitionException {
		Decimal_valueContext _localctx = new Decimal_valueContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_decimal_value);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(24);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__0) {
				{
				setState(23);
				match(T__0);
				}
			}

			setState(26);
			match(Digits);
			setState(29);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==T__1) {
				{
				setState(27);
				match(T__1);
				setState(28);
				match(Digits);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Boolean_valueContext extends ParserRuleContext {
		public TerminalNode TRUE() { return getToken(BindableParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(BindableParser.FALSE, 0); }
		public Boolean_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_boolean_value; }
	}

	public final Boolean_valueContext boolean_value() throws RecognitionException {
		Boolean_valueContext _localctx = new Boolean_valueContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_boolean_value);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(31);
			_la = _input.LA(1);
			if ( !(_la==TRUE || _la==FALSE) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Namespace_qualifierContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(BindableParser.IDENTIFIER, 0); }
		public Namespace_qualifierContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_namespace_qualifier; }
	}

	public final Namespace_qualifierContext namespace_qualifier() throws RecognitionException {
		Namespace_qualifierContext _localctx = new Namespace_qualifierContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_namespace_qualifier);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(33);
			match(IDENTIFIER);
			setState(34);
			match(T__2);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Static_typeContext extends ParserRuleContext {
		public Namespace_qualifierContext namespace_qualifier() {
			return getRuleContext(Namespace_qualifierContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(BindableParser.IDENTIFIER, 0); }
		public Static_typeContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_static_type; }
	}

	public final Static_typeContext static_type() throws RecognitionException {
		Static_typeContext _localctx = new Static_typeContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_static_type);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(36);
			namespace_qualifier();
			setState(37);
			match(IDENTIFIER);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Attached_exprContext extends ParserRuleContext {
		public Static_typeContext static_type() {
			return getRuleContext(Static_typeContext.class,0);
		}
		public List<TerminalNode> IDENTIFIER() { return getTokens(BindableParser.IDENTIFIER); }
		public TerminalNode IDENTIFIER(int i) {
			return getToken(BindableParser.IDENTIFIER, i);
		}
		public Attached_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attached_expr; }
	}

	public final Attached_exprContext attached_expr() throws RecognitionException {
		Attached_exprContext _localctx = new Attached_exprContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_attached_expr);
		try {
			setState(50);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(39);
				match(T__3);
				setState(40);
				static_type();
				setState(41);
				match(T__1);
				setState(42);
				match(IDENTIFIER);
				setState(43);
				match(T__4);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(45);
				match(T__3);
				setState(46);
				match(IDENTIFIER);
				setState(47);
				match(T__1);
				setState(48);
				match(IDENTIFIER);
				setState(49);
				match(T__4);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Cast_exprContext extends ParserRuleContext {
		public Static_typeContext static_type() {
			return getRuleContext(Static_typeContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(BindableParser.IDENTIFIER, 0); }
		public Cast_exprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_cast_expr; }
	}

	public final Cast_exprContext cast_expr() throws RecognitionException {
		Cast_exprContext _localctx = new Cast_exprContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_cast_expr);
		try {
			setState(59);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,3,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(52);
				match(T__3);
				setState(53);
				static_type();
				setState(54);
				match(T__4);
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(56);
				match(T__3);
				setState(57);
				match(IDENTIFIER);
				setState(58);
				match(T__4);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionContext extends ParserRuleContext {
		public TerminalNode IDENTIFIER() { return getToken(BindableParser.IDENTIFIER, 0); }
		public List<Function_paramContext> function_param() {
			return getRuleContexts(Function_paramContext.class);
		}
		public Function_paramContext function_param(int i) {
			return getRuleContext(Function_paramContext.class,i);
		}
		public FunctionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function; }
	}

	public final FunctionContext function() throws RecognitionException {
		FunctionContext _localctx = new FunctionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_function);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(61);
			match(IDENTIFIER);
			setState(62);
			match(T__3);
			setState(64);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__3) | (1L << IDENTIFIER) | (1L << TRUE) | (1L << FALSE) | (1L << NULL) | (1L << Digits) | (1L << QuotedString))) != 0)) {
				{
				setState(63);
				function_param();
				}
			}

			setState(70);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==T__5) {
				{
				{
				setState(66);
				match(T__5);
				setState(67);
				function_param();
				}
				}
				setState(72);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(73);
			match(T__4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class PathContext extends ParserRuleContext {
		public PathContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_path; }
	 
		public PathContext() { }
		public void copyFrom(PathContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class PathStaticFuctionContext extends PathContext {
		public Static_typeContext static_type() {
			return getRuleContext(Static_typeContext.class,0);
		}
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public PathStaticFuctionContext(PathContext ctx) { copyFrom(ctx); }
	}
	public static class PathCastContext extends PathContext {
		public Cast_exprContext cast_expr() {
			return getRuleContext(Cast_exprContext.class,0);
		}
		public PathCastContext(PathContext ctx) { copyFrom(ctx); }
	}
	public static class PathPathToFunctionContext extends PathContext {
		public PathContext path() {
			return getRuleContext(PathContext.class,0);
		}
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public PathPathToFunctionContext(PathContext ctx) { copyFrom(ctx); }
	}
	public static class PathIndexerContext extends PathContext {
		public PathContext path() {
			return getRuleContext(PathContext.class,0);
		}
		public TerminalNode Digits() { return getToken(BindableParser.Digits, 0); }
		public PathIndexerContext(PathContext ctx) { copyFrom(ctx); }
	}
	public static class PathCastInvalidContext extends PathContext {
		public Attached_exprContext attached_expr() {
			return getRuleContext(Attached_exprContext.class,0);
		}
		public PathCastInvalidContext(PathContext ctx) { copyFrom(ctx); }
	}
	public static class PathCastPathParenContext extends PathContext {
		public Cast_exprContext cast_expr() {
			return getRuleContext(Cast_exprContext.class,0);
		}
		public PathContext path() {
			return getRuleContext(PathContext.class,0);
		}
		public PathCastPathParenContext(PathContext ctx) { copyFrom(ctx); }
	}
	public static class PathFunctionContext extends PathContext {
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public PathFunctionContext(PathContext ctx) { copyFrom(ctx); }
	}
	public static class PathStringIndexerContext extends PathContext {
		public PathContext path() {
			return getRuleContext(PathContext.class,0);
		}
		public TerminalNode QuotedString() { return getToken(BindableParser.QuotedString, 0); }
		public PathStringIndexerContext(PathContext ctx) { copyFrom(ctx); }
	}
	public static class PathIdentifierContext extends PathContext {
		public TerminalNode IDENTIFIER() { return getToken(BindableParser.IDENTIFIER, 0); }
		public PathIdentifierContext(PathContext ctx) { copyFrom(ctx); }
	}
	public static class PathCastPathContext extends PathContext {
		public Cast_exprContext cast_expr() {
			return getRuleContext(Cast_exprContext.class,0);
		}
		public PathContext path() {
			return getRuleContext(PathContext.class,0);
		}
		public PathCastPathContext(PathContext ctx) { copyFrom(ctx); }
	}
	public static class PathStaticIdentifierContext extends PathContext {
		public Static_typeContext static_type() {
			return getRuleContext(Static_typeContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(BindableParser.IDENTIFIER, 0); }
		public PathStaticIdentifierContext(PathContext ctx) { copyFrom(ctx); }
	}
	public static class PathDotIdentifierContext extends PathContext {
		public PathContext path() {
			return getRuleContext(PathContext.class,0);
		}
		public TerminalNode IDENTIFIER() { return getToken(BindableParser.IDENTIFIER, 0); }
		public PathDotIdentifierContext(PathContext ctx) { copyFrom(ctx); }
	}
	public static class PathDotAttachedContext extends PathContext {
		public PathContext path() {
			return getRuleContext(PathContext.class,0);
		}
		public Attached_exprContext attached_expr() {
			return getRuleContext(Attached_exprContext.class,0);
		}
		public PathDotAttachedContext(PathContext ctx) { copyFrom(ctx); }
	}

	public final PathContext path() throws RecognitionException {
		return path(0);
	}

	private PathContext path(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		PathContext _localctx = new PathContext(_ctx, _parentState);
		PathContext _prevctx = _localctx;
		int _startState = 16;
		enterRecursionRule(_localctx, 16, RULE_path, _p);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(96);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				{
				_localctx = new PathIdentifierContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(76);
				match(IDENTIFIER);
				}
				break;
			case 2:
				{
				_localctx = new PathStaticIdentifierContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(77);
				static_type();
				setState(78);
				match(T__1);
				setState(79);
				match(IDENTIFIER);
				}
				break;
			case 3:
				{
				_localctx = new PathCastInvalidContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(81);
				attached_expr();
				}
				break;
			case 4:
				{
				_localctx = new PathCastContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(82);
				cast_expr();
				}
				break;
			case 5:
				{
				_localctx = new PathCastPathContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(83);
				cast_expr();
				setState(84);
				path(5);
				}
				break;
			case 6:
				{
				_localctx = new PathCastPathParenContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(86);
				match(T__3);
				setState(87);
				cast_expr();
				setState(88);
				path(0);
				setState(89);
				match(T__4);
				}
				break;
			case 7:
				{
				_localctx = new PathFunctionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(91);
				function();
				}
				break;
			case 8:
				{
				_localctx = new PathStaticFuctionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(92);
				static_type();
				setState(93);
				match(T__1);
				setState(94);
				function();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(117);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(115);
					_errHandler.sync(this);
					switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
					case 1:
						{
						_localctx = new PathDotIdentifierContext(new PathContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_path);
						setState(98);
						if (!(precpred(_ctx, 12))) throw new FailedPredicateException(this, "precpred(_ctx, 12)");
						setState(99);
						match(T__1);
						setState(100);
						match(IDENTIFIER);
						}
						break;
					case 2:
						{
						_localctx = new PathIndexerContext(new PathContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_path);
						setState(101);
						if (!(precpred(_ctx, 10))) throw new FailedPredicateException(this, "precpred(_ctx, 10)");
						setState(102);
						match(T__6);
						setState(103);
						match(Digits);
						setState(104);
						match(T__7);
						}
						break;
					case 3:
						{
						_localctx = new PathStringIndexerContext(new PathContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_path);
						setState(105);
						if (!(precpred(_ctx, 9))) throw new FailedPredicateException(this, "precpred(_ctx, 9)");
						setState(106);
						match(T__6);
						setState(107);
						match(QuotedString);
						setState(108);
						match(T__7);
						}
						break;
					case 4:
						{
						_localctx = new PathDotAttachedContext(new PathContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_path);
						setState(109);
						if (!(precpred(_ctx, 8))) throw new FailedPredicateException(this, "precpred(_ctx, 8)");
						setState(110);
						match(T__1);
						setState(111);
						attached_expr();
						}
						break;
					case 5:
						{
						_localctx = new PathPathToFunctionContext(new PathContext(_parentctx, _parentState));
						pushNewRecursionContext(_localctx, _startState, RULE_path);
						setState(112);
						if (!(precpred(_ctx, 2))) throw new FailedPredicateException(this, "precpred(_ctx, 2)");
						setState(113);
						match(T__1);
						setState(114);
						function();
						}
						break;
					}
					} 
				}
				setState(119);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,8,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Function_paramContext extends ParserRuleContext {
		public Function_paramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_param; }
	 
		public Function_paramContext() { }
		public void copyFrom(Function_paramContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class FunctionParamNumberContext extends Function_paramContext {
		public Decimal_valueContext decimal_value() {
			return getRuleContext(Decimal_valueContext.class,0);
		}
		public FunctionParamNumberContext(Function_paramContext ctx) { copyFrom(ctx); }
	}
	public static class FunctionParamBoolContext extends Function_paramContext {
		public Boolean_valueContext boolean_value() {
			return getRuleContext(Boolean_valueContext.class,0);
		}
		public FunctionParamBoolContext(Function_paramContext ctx) { copyFrom(ctx); }
	}
	public static class FunctionParameterInvalidContext extends Function_paramContext {
		public FunctionContext function() {
			return getRuleContext(FunctionContext.class,0);
		}
		public FunctionParameterInvalidContext(Function_paramContext ctx) { copyFrom(ctx); }
	}
	public static class FunctionParamStringContext extends Function_paramContext {
		public TerminalNode QuotedString() { return getToken(BindableParser.QuotedString, 0); }
		public FunctionParamStringContext(Function_paramContext ctx) { copyFrom(ctx); }
	}
	public static class FunctionParamPathContext extends Function_paramContext {
		public PathContext path() {
			return getRuleContext(PathContext.class,0);
		}
		public FunctionParamPathContext(Function_paramContext ctx) { copyFrom(ctx); }
	}
	public static class FunctionParamNullValueContext extends Function_paramContext {
		public TerminalNode NULL() { return getToken(BindableParser.NULL, 0); }
		public FunctionParamNullValueContext(Function_paramContext ctx) { copyFrom(ctx); }
	}

	public final Function_paramContext function_param() throws RecognitionException {
		Function_paramContext _localctx = new Function_paramContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_function_param);
		try {
			setState(126);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				_localctx = new FunctionParameterInvalidContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(120);
				function();
				}
				break;
			case 2:
				_localctx = new FunctionParamPathContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(121);
				path(0);
				}
				break;
			case 3:
				_localctx = new FunctionParamBoolContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(122);
				boolean_value();
				}
				break;
			case 4:
				_localctx = new FunctionParamNumberContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(123);
				decimal_value();
				}
				break;
			case 5:
				_localctx = new FunctionParamStringContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(124);
				match(QuotedString);
				}
				break;
			case 6:
				_localctx = new FunctionParamNullValueContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(125);
				match(NULL);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 8:
			return path_sempred((PathContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean path_sempred(PathContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 12);
		case 1:
			return precpred(_ctx, 10);
		case 2:
			return precpred(_ctx, 9);
		case 3:
			return precpred(_ctx, 8);
		case 4:
			return precpred(_ctx, 2);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\24\u0083\4\2\t\2"+
		"\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\3\2\3\2\3\2\3\3\5\3\33\n\3\3\3\3\3\3\3\5\3 \n\3\3\4\3\4\3\5\3\5"+
		"\3\5\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7\65\n"+
		"\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b>\n\b\3\t\3\t\3\t\5\tC\n\t\3\t\3\t\7"+
		"\tG\n\t\f\t\16\tJ\13\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\nc\n\n\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\7\nv\n\n\f\n\16\n"+
		"y\13\n\3\13\3\13\3\13\3\13\3\13\3\13\5\13\u0081\n\13\3\13\2\3\22\f\2\4"+
		"\6\b\n\f\16\20\22\24\2\3\3\2\20\21\2\u008f\2\26\3\2\2\2\4\32\3\2\2\2\6"+
		"!\3\2\2\2\b#\3\2\2\2\n&\3\2\2\2\f\64\3\2\2\2\16=\3\2\2\2\20?\3\2\2\2\22"+
		"b\3\2\2\2\24\u0080\3\2\2\2\26\27\5\22\n\2\27\30\7\2\2\3\30\3\3\2\2\2\31"+
		"\33\7\3\2\2\32\31\3\2\2\2\32\33\3\2\2\2\33\34\3\2\2\2\34\37\7\23\2\2\35"+
		"\36\7\4\2\2\36 \7\23\2\2\37\35\3\2\2\2\37 \3\2\2\2 \5\3\2\2\2!\"\t\2\2"+
		"\2\"\7\3\2\2\2#$\7\13\2\2$%\7\5\2\2%\t\3\2\2\2&\'\5\b\5\2\'(\7\13\2\2"+
		"(\13\3\2\2\2)*\7\6\2\2*+\5\n\6\2+,\7\4\2\2,-\7\13\2\2-.\7\7\2\2.\65\3"+
		"\2\2\2/\60\7\6\2\2\60\61\7\13\2\2\61\62\7\4\2\2\62\63\7\13\2\2\63\65\7"+
		"\7\2\2\64)\3\2\2\2\64/\3\2\2\2\65\r\3\2\2\2\66\67\7\6\2\2\678\5\n\6\2"+
		"89\7\7\2\29>\3\2\2\2:;\7\6\2\2;<\7\13\2\2<>\7\7\2\2=\66\3\2\2\2=:\3\2"+
		"\2\2>\17\3\2\2\2?@\7\13\2\2@B\7\6\2\2AC\5\24\13\2BA\3\2\2\2BC\3\2\2\2"+
		"CH\3\2\2\2DE\7\b\2\2EG\5\24\13\2FD\3\2\2\2GJ\3\2\2\2HF\3\2\2\2HI\3\2\2"+
		"\2IK\3\2\2\2JH\3\2\2\2KL\7\7\2\2L\21\3\2\2\2MN\b\n\1\2Nc\7\13\2\2OP\5"+
		"\n\6\2PQ\7\4\2\2QR\7\13\2\2Rc\3\2\2\2Sc\5\f\7\2Tc\5\16\b\2UV\5\16\b\2"+
		"VW\5\22\n\7Wc\3\2\2\2XY\7\6\2\2YZ\5\16\b\2Z[\5\22\n\2[\\\7\7\2\2\\c\3"+
		"\2\2\2]c\5\20\t\2^_\5\n\6\2_`\7\4\2\2`a\5\20\t\2ac\3\2\2\2bM\3\2\2\2b"+
		"O\3\2\2\2bS\3\2\2\2bT\3\2\2\2bU\3\2\2\2bX\3\2\2\2b]\3\2\2\2b^\3\2\2\2"+
		"cw\3\2\2\2de\f\16\2\2ef\7\4\2\2fv\7\13\2\2gh\f\f\2\2hi\7\t\2\2ij\7\23"+
		"\2\2jv\7\n\2\2kl\f\13\2\2lm\7\t\2\2mn\7\24\2\2nv\7\n\2\2op\f\n\2\2pq\7"+
		"\4\2\2qv\5\f\7\2rs\f\4\2\2st\7\4\2\2tv\5\20\t\2ud\3\2\2\2ug\3\2\2\2uk"+
		"\3\2\2\2uo\3\2\2\2ur\3\2\2\2vy\3\2\2\2wu\3\2\2\2wx\3\2\2\2x\23\3\2\2\2"+
		"yw\3\2\2\2z\u0081\5\20\t\2{\u0081\5\22\n\2|\u0081\5\6\4\2}\u0081\5\4\3"+
		"\2~\u0081\7\24\2\2\177\u0081\7\22\2\2\u0080z\3\2\2\2\u0080{\3\2\2\2\u0080"+
		"|\3\2\2\2\u0080}\3\2\2\2\u0080~\3\2\2\2\u0080\177\3\2\2\2\u0081\25\3\2"+
		"\2\2\f\32\37\64=BHbuw\u0080";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}