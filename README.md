# Bindable

- [Pre-proposal doc](https://docs.google.com/document/d/10mgTMsZ2RJ20wlYInY2OWDyqs_Llk0ROE8GOXTTST3I/edit?usp=sharing)
- [Proposal doc](https://docs.google.com/document/d/1dfbY-CrMBk5C2EaRHVF4P8PucZ9eIxGBII5LxFb2TFE/edit?usp=sharing)
- [Design doc](https://docs.google.com/document/d/1ABJIz2-aqQHNDCgr_jEaN09VoNDBAbLZ1ba_5XHY000/edit?usp=sharing)
- [Final Report](https://docs.google.com/document/d/16n5wKRBYtd_ccw9zFs3dPBITBDdfZM0FiSU3kr81zcg/edit?usp=sharing)