const antlr4 = require("antlr4");
const BindableLexer = require("../gen/BindableLexer").BindableLexer;
const BindableParser = require("../gen/BindableParser").BindableParser;
const BindableParserListener = require("../gen/BindableParserListener").BindableParserListener;

function escapeString(s) {
    return ("" + s).replace(/["'\\\n\r\u2028\u2029]/g, (c) => {
        switch (c) {
            case '"':
            case "'":
            case "\\":
                return "\\" + c;
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\u2028":
                return "\\u2028";
            case "\u2029":
                return "\\u2029";
        }
    });
}

class BindableCompiler extends BindableParserListener {
    constructor() {
        super();
        this._inputVar = "_$ctx";
        this._outputVar = "_$result";
        this._helpers = {expr: {}, block: {}};
        this._usedHelpers = {expr: [], block: []};

        // 
        // Register standard block helpers: #each, #if, #with
        // 

        // {{#each list}} — takes as input an expression that evaluates to a list, 
        // iterates over it, invokes the body template with each list element as a 
        // data context, and concatenates the results.
        this.registerBlockHelper('each', function (ctx, body, list) {
            var output = "";
			for (element in list) {
				output += body(list[element]);
			}
			return output;
        });

        // {{#if expr}} — takes as input any expression. If it is truthy, invokes 
        // the body template without changing the data context and returns the result; 
        // otherwise, returns an empty string.
        this.registerBlockHelper('if', function (ctx, body, condition) {
            var output = "";
			if (condition) {
				output += body(ctx);
			}
			return output;
        });

        // {{#with 'field'}} — takes as input an expression that evaluates to a 
        // string. This string is assumed to be a field name on the current data 
        // context. The helper changes the data context to the value of that field, 
        // invokes the body template on it, and returns the result.
        this.registerBlockHelper('with', function (ctx, body, field) {
			return body(ctx[field]);
        });
    }

    // When you define a helper in the generated JavaScript function as a local variable 
    // (“var helperName = function(…) { … }”), you need to make sure that its name does 
    // not conflict with any of the JavaScript reserved words, or the function won’t 
    // compile. If the user defines a helper called “if”, and you represent it in the 
    // generated function as a local variable called “if”, the call to new Function will 
    // throw a compilation error. To fix this, mangle the name of a variable representing 
    // each helper (e.g. add a prefix to it that will resolve any conflicts). Remember
    // that you need to use the mangled helper name consistently both in its definition 
    // and in its usage.
    mangleHelperName(name) {
        return `__$${name}`;
    }

    registerExprHelper(name, helper) {
        this._helpers.expr[name] = helper;
    }

    registerBlockHelper(name, helper) {
        this._helpers.block[name] = helper;
    }

    generateHelperDefinitions() {
        var helperDefinitions = '';

        // Generate the helper definitions for used expr helpers
        for (var exprHelperName of this._usedHelpers.expr) {
            var exprHelperBody = this._helpers.expr[exprHelperName];
            helperDefinitions += `\nvar ${this.mangleHelperName(exprHelperName)} = ${exprHelperBody.toString()}\n`;
        }

        // Generate the helper definitions for used block helpers
        for (var blockHelperName of this._usedHelpers.block) {
            var blockHelperBody = this._helpers.block[blockHelperName];
            helperDefinitions += `\nvar ${this.mangleHelperName(blockHelperName)} = ${blockHelperBody.toString()}\n`;
        }

        // Prepend the helper definitions
        this._bodyStack[this._bodyStack.length -1] = helperDefinitions + this._bodyStack[this._bodyStack.length -1];
    }
    
    pushScope() {
        this._bodyStack.push(`var ${this._outputVar} = "";\n`);
    }

    popScope() {
        this._bodyStack[this._bodyStack.length -1] += `return ${this._outputVar};\n`;
        return this._bodyStack.pop();
    }

    compile(template) {
        this._bodyStack = [];
        this.pushScope();

        const chars = new antlr4.InputStream(template);
        const lexer = new BindableLexer(chars);
        const tokens = new antlr4.CommonTokenStream(lexer);
        const parser = new BindableParser(tokens);
        parser.buildParseTrees = true;
        const tree = parser.document();
        antlr4.tree.ParseTreeWalker.DEFAULT.walk(this, tree);

        this.generateHelperDefinitions();

        var resultCode = this.popScope();
        
        return new Function(this._inputVar, resultCode);
    }

    append(expr) {
        this._bodyStack[this._bodyStack.length -1] += `${this._outputVar} += ${expr};\n`;
    }

    exitRawElement(ctx) {
        this.append(`"${escapeString(ctx.getText())}"`);
    }

    exitLiteral(ctx) {
        ctx.source = `${ctx.getText()}`;
    }

    exitIdentifier(ctx) {
        ctx.source = `${this._inputVar + "." + ctx.varName.text}`;
    }

    exitParenthesizedExpression(ctx) {
        ctx.source = `${ctx.exp.source}`;
    }

    exitExpression(ctx) {
        ctx.source = `${ctx.children[0].source}`;
    }

    exitExpressionElement(ctx) {
        this.append(`${ctx.exp.source}`);
    }

    exitHelperParam(ctx) {
        ctx.source = ctx.children[0].source;
    }

    exitExprHelper(ctx) {
        var exprHelperName = ctx.exprHelperName.text;
        if (!this._usedHelpers.expr.includes(exprHelperName)) {
            this._usedHelpers.expr.push(exprHelperName);
        }

        ctx.source = `${this.mangleHelperName(exprHelperName)}(${this._inputVar}`;
        for (var i = 0; i < ctx.params.length; i++) {
            ctx.source += `, ${ctx.params[i].source}`;
        }
        ctx.source += ')';
    }
    
    enterBlockBody(ctx) {
        this.pushScope();
    }

    exitBlockBody(ctx) {
        var blockBodyCode = this.popScope();
        ctx.source = (new Function(this._inputVar, blockBodyCode)).toString();
    }

    exitBlockElement(ctx) {
        // Throw an error for non-matching opening and closing blocks
        if (ctx.opening.blockHelperName.text != ctx.closing.blockHelperName.text) {
            throw `Block start '${ctx.opening.blockHelperName.text}' does not match the block end '${ctx.closing.blockHelperName.text}'.`;
        }

        var blockHelperName = ctx.opening.blockHelperName.text;
        if (!this._usedHelpers.block.includes(blockHelperName)) {
            this._usedHelpers.block.push(blockHelperName);
        }

        var blockCode = `${this.mangleHelperName(blockHelperName)}(${this._inputVar},${ctx.body.source}`;
        for (var i = 0; i < ctx.opening.params.length; i++) {
            blockCode += `,${ctx.opening.params[i].source}`;
        }
        blockCode += ')';

        this.append(blockCode);
    }
}

exports.BindableCompiler = BindableCompiler;
